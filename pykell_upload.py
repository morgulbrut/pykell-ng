#!/usr/bin/env python3
'''
2017 Tillo Bosshart
'''
import pykell_log, ftplib


class FTP_uploader(object):
    """docstring for FPU_uploader"""
    def __init__(self, server,username=None,password=None,folder=None):
        super(FTP_uploader, self).__init__()
        self.server = server
        self.folder = folder
        self.username = username
        self.password = password
        self.log = pykell_log.Conslog()

    def upload(self,file):
        with open(file,'rb') as f:
            self.log.info('Conneting as {} to {}'.format(self.username,self.server))
            if self.username:
                session = ftplib.FTP(self.server,self.username,self.password)
            else:
                session = ftplib.FTP(self.server)
            if self.folder:
                ftp_cmd = 'STOR ' + self.folder + '/' + file
            else:
                ftp_cmd = 'STOR /' + file
            session.login() 
            self.log.info(ftp_cmd)
            session.storlines(ftp_cmd,f)
            session.quit()
        

def test():
    ftp = FTP_uploader('speedtest.tele2.net')
    ftp.upload('test.html')


if __name__ == '__main__':
    test()