#!/usr/bin/env python3
'''
2017 Tillo Bosshart
'''
import logging



class Conslog(object):
    """docstring for Conslog"""
    def __init__(self):
        super(Conslog, self).__init__()
        self.HEADER = '\033[95m'
        self.OKBLUE = '\033[94m'
        self.OKGREEN = '\033[92m'
        self.WARNING = '\033[93m'
        self.FAIL = '\033[91m'
        self.ENDC = '\033[0m'
        self.BOLD = '\033[1m'
        self.UNDERLINE = '\033[4m'
 

    def error(self,str):
        print(self.FAIL + str + self.ENDC)

    def warning(self,str):  
        print(self.WARNING + str + self.ENDC)

    def success(self,str):
        print(self.OKGREEN + str + self.ENDC)

    def info(self,str):
        print(self.OKBLUE + str + self.ENDC)

'''
class Filelog(object):
    """docstring for Conslog"""
    def __init__(self, log='log.txt'):
        super(Conslog, self).__init__()
        self logfile=log

    def error(self,str):
        

    def warning(self,str):  
        print(self.WARNING + str + self.ENDC)

    def success(self,str):
        print(self.OKGREEN + str + self.ENDC)

    def info(self,str):
        print(self.OKBLUE + str + self.ENDC)
'''

# to test run the module itself(maybe not the best solution)

def test():
    log = Conslog()
    log.error('ERROR')
    log.warning('WARNING')
    log.success('SUCCESS')
    log.info('INFO')
    log = Filelog()


if __name__ == '__main__':
    test()